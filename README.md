# publicis-sapient

```text
Task:
Containers
- Using your favourite configuration management tool(s) write the code to:
    1.	Provision a cluster on your preferred cloud provider (AWS, Azure, GCP) to deploy a container
    2.	Build a container image for a stateless web application that for your chosen platform (Nodejs, Java, .Net)
    3.	Deploy the image on the kubernetes cluster
    4.	Expose the application to the internet and provision any necessary cloud provider specific resources (if required)
```

### Explanation:

1. I can not show You exact cluster creation process due to offline exam type.
But i would use **terraform** for that, as it is my favorite infra management tool.
There are a lot of ready scripts around. I'd start with [this one](https://github.com/terraform-aws-modules/terraform-aws-eks/tree/master/examples/complete).
    It can provision full blown EKS cluster.

On my private cloud machine i have installed [KIND](https://kind.sigs.k8s.io/), which is a binary to run Kubernetes IN Docker.
   - Also i have [LoadBalancer](https://kind.sigs.k8s.io/docs/user/loadbalancer/) and [Nginx](https://kind.sigs.k8s.io/docs/user/ingress/) ingress controller installed.

2. I decided to go with [basic example nodejs app](https://nodejs.org/en/docs/guides/nodejs-docker-webapp) 
    - All Files are located in `fe` directory.
    - I created a Dockerfile:
   ```dockerfile
   FROM node:lts-alpine
   WORKDIR /usr/src/app
   COPY fe/*.* ./
   RUN npm install
   COPY . .
   EXPOSE 8080
   CMD [ "node", "server.js" ]
    ```
   - build it with `docker build -t registry.gitlab.com/noizo/publicis-sapient .` command. 
   - stored image: `docker push registry.gitlab.com/noizo/publicis-sapient`
3. I have created `deploy.yaml` to deploy app to a cluster and applied it with `kubectl apply -f deploy.yaml`
   - Deploy.yaml contain sections:
      - deployment (to deploy nodejs app)
      - service (to expose app port from cluster to machine)
      - ingress (to expose service to the world)
4. Exposing is provided through nginx ingress controller within `deploy.yaml` file.

You can access app on http://141.144.235.34:80


I would usually use some deployment tool to deploy apps. Helm or whaever automation tool around.

I would usually provide some encryption for ingress/domain of course, but this is not the case in this scenario.
