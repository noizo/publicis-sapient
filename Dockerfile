FROM node:lts-alpine
WORKDIR /usr/src/app
COPY fe/*.* ./
RUN npm install
COPY . .
EXPOSE 8080
CMD [ "node", "server.js" ]
